//
//  PhotoListViewModelTests.swift
//  FlickrFeedTests
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Quick
import Nimble
@testable import FlickrFeed

class PhotoListViewModelSpec: QuickSpec {

    class MockPhotoListViewModelDelegate: PhotoListViewModelDelegate {
        private(set) var viewStates: [PhotoListViewModel.ViewState] = []
        func viewModelDidChangeStateTo(_ viewState: PhotoListViewModel.ViewState) {
            self.viewStates.append(viewState)
        }
    }

    class MockFlickrFeedDownloader: FlickrFeedDownloader {
        var photos: [FlickrPhoto] = []
        override func downloadFeed() -> [FlickrPhoto] {
            return photos
        }
    }

    override func spec() {
        var mockFlickrFeedDownloader: MockFlickrFeedDownloader!
        var mockPhotoListViewModelDelegate: MockPhotoListViewModelDelegate!
        var sut: PhotoListViewModel!

        beforeEach {
            mockFlickrFeedDownloader = MockFlickrFeedDownloader()
            sut = PhotoListViewModel(flickrFeedDownloader: mockFlickrFeedDownloader)

            mockPhotoListViewModelDelegate = MockPhotoListViewModelDelegate()
            sut.delegate = mockPhotoListViewModelDelegate
        }

        describe("photo loading") {
            context("not started yet") {
                it("has state init") {
                    expect(mockPhotoListViewModelDelegate.viewStates).to(equal([.`init`]))
                }

                it("has no items") {
                    expect(sut.numberOfItems).to(equal(0))
                }

                it("has empty photo list") {
                    expect(sut.photos).to(beEmpty())
                }
            }
            context("no photos loaded") {
                beforeEach {
                    sut.load()
                }

                it("has moved through all states and returned empty results") {
                    expect(mockPhotoListViewModelDelegate.viewStates).toEventually(equal([.`init`, .loading, .empty]))
                    expect(sut.numberOfItems).to(equal(0))
                    expect(sut.photos).to(beEmpty())
                }
            }

            context("some photos loaded") {
                beforeEach {
                    mockFlickrFeedDownloader.photos = [FlickrPhoto(URL: URL(string: "https://google.com")!, title: nil, author: nil, aspectRatio: nil)]
                    sut.load()
                }

                it("has state init") {
                    expect(mockPhotoListViewModelDelegate.viewStates).toEventually(equal([.`init`, .loading, .ready]))
                }

                it("has no items") {
                    expect(sut.numberOfItems).toEventually(equal(1))
                }

                it("has empty photo list") {
                    expect(sut.photos).toEventually(equal(mockFlickrFeedDownloader.photos))
                }

            }
        }
    }
}
