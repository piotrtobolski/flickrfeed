//
//  NSStringUtilitiesTests.swift
//  FlickrFeedTests
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Quick
import Nimble
@testable import FlickrFeed

class NSStringUtilitiesSpec: QuickSpec {

    override func spec() {
        var sut: NSString!
        
        describe("getFloat") {
            var returnedValue: Float?

            context("there is number in the range") {
                beforeEach {
                    sut = "1234567890"
                    returnedValue = sut.getFloat(in: NSRange(location: 5, length: 3))
                }

                it("parses float correctly") {
                    expect(returnedValue).to(equal(678))
                }
            }

            context("there is no number in the range") {
                beforeEach {
                    sut = "123456x890"
                    returnedValue = sut.getFloat(in: NSRange(location: 5, length: 3))
                }

                it("parses float correctly") {
                    expect(returnedValue).to(beNil())
                }
            }
        }
    }
}
