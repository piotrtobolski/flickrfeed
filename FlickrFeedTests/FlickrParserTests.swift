//
//  FlickrParserTests.swift
//  FlickrFeedTests
//
//  Created by Piotr Tobolski on 02.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Quick
import Nimble
import FeedKit
@testable import FlickrFeed

class FlickrParserSpec: QuickSpec {
    override func spec() {
        var sut: FlickrParser!

        beforeEach {
            sut = FlickrParser()
        }

        describe("photo list parsing") {
            var photos: [FlickrPhoto]!

            context("feed is missing") {
                beforeEach {
                    photos = sut.getPhotos()
                }

                it("returns no photos") {
                    expect(photos).to(beEmpty())
                }
            }

            context("feed contains full response") {
                beforeEach {
                    sut.feed = atomFeedFromFile("ResponseFull")
                    photos = sut.getPhotos()
                }

                it("parsed 20 photos") {
                    expect(photos).to(haveCount(20))
                }

                describe("3rd photo") {
                    var photo: FlickrPhoto!

                    beforeEach {
                        photo = photos[2]
                    }

                    it("has correct title") {
                        expect(photo.title).to(equal("DSC_0114.JPG"))
                    }

                    it("has correct author") {
                        expect(photo.author).to(equal("Hoopswithanikon"))
                    }

                    it("has correct aspect ratio") {
                        expect(photo.aspectRatio).to(equal(1.5))
                    }

                    it("has correct URL") {
                        expect(photo.URL.absoluteString).to(equal("https://farm5.staticflickr.com/4664/26171709188_3173783d82_b.jpg"))
                    }
                }
            }

            context("feed contains response with missing values") {
                beforeEach {
                    sut.feed = atomFeedFromFile("ResponsePartial")
                    photos = sut.getPhotos()
                }

                it("parsed 2 photos") {
                    expect(photos).to(haveCount(2))
                }

                describe("first photo") {
                    var photo: FlickrPhoto!

                    beforeEach {
                        photo = photos.first
                    }

                    it("has correct title") {
                        expect(photo.title).to(equal("title2"))
                    }

                    it("has correct author") {
                        expect(photo.author).to(equal("author2"))
                    }

                    it("has correct aspect ratio") {
                        expect(photo.aspectRatio).to(equal(0.5))
                    }

                    it("has correct URL") {
                        expect(photo.URL.absoluteString).to(equal("https://2"))
                    }
                }

                describe("last photo") {
                    var photo: FlickrPhoto!

                    beforeEach {
                        photo = photos.last
                    }

                    it("has no title") {
                        expect(photo.title).to(beNil())
                    }

                    it("has no author") {
                        expect(photo.author).to(beNil())
                    }

                    it("has no aspect ratio") {
                        expect(photo.aspectRatio).to(beNil())
                    }

                    it("has correct URL") {
                        expect(photo.URL.absoluteString).to(equal("https://3"))
                    }

                }
            }
        }
    }
}
