//
//  FlickrFeedFixture.swift
//  FlickrFeedTests
//
//  Created by Piotr Tobolski on 02.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Foundation
import FeedKit

func atomFeedFromFile(_ named: String) -> AtomFeed {
    class DummyClass {}
    let bundle = Bundle(for: DummyClass.self)
    let filePath = bundle.path(forResource: named, ofType: "xml")!
    let url = URL(fileURLWithPath: filePath)
    let parser = FeedParser(URL: url)!
    let feed = parser.parse().atomFeed
    return feed!
}
