//
//  FlickrFeedDownloaderTests.swift
//  FlickrFeedTests
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Quick
import Nimble
import FeedKit
@testable import FlickrFeed

class FlickrFeedDownloaderSpec: QuickSpec {

    class MockFeedParser: FeedParserProtocol {
        var feed: AtomFeed?

        init() {}

        func parse() -> Result {
            guard let feed = feed else {
                return .failure(NSError(domain: "", code: 0, userInfo: nil))
            }

            return .atom(feed)
        }
    }

    class MockFlickrParser: FlickrParser {
        var photos: [FlickrPhoto] = []
        override func getPhotos() -> [FlickrPhoto] {
            return photos
        }
    }

    override func spec() {
        var mockFeedParser: MockFeedParser!
        var mockFlickrParser: MockFlickrParser!
        var sut: FlickrFeedDownloader!
        var feedParserCreateBlockCalled = false

        beforeEach {
            mockFeedParser = MockFeedParser()
            mockFlickrParser = MockFlickrParser()
            sut = FlickrFeedDownloader(feedParserCreateBlock: { feedParserCreateBlockCalled = true; return mockFeedParser },
                                       flickrParser: mockFlickrParser)
        }

        it("doesn't call feedParserCreateBlock before downloading feed") {
            expect(feedParserCreateBlockCalled).to(beFalsy())
        }

        describe("downloading feed") {
            var downloadedPhotos: [FlickrPhoto]?

            context("feed download failed") {
                beforeEach {
                    downloadedPhotos = sut.downloadFeed()
                }

                it("returns empty array") {
                    expect(downloadedPhotos).to(beEmpty())
                }
            }

            context("correct feed provided") {
                beforeEach {
                    mockFeedParser.feed = atomFeedFromFile("ResponseFull")
                    mockFlickrParser.photos = [FlickrPhoto(URL: URL(string: "https://google.com")!, title: nil, author: nil, aspectRatio: nil)]
                    downloadedPhotos = sut.downloadFeed()
                }

                it("passes the feed to flickrParser") {
                    expect(mockFlickrParser.feed).to(beIdenticalTo(mockFeedParser.feed))
                }

                it("gets photos from flickr parser") {
                    expect(downloadedPhotos).to(equal(mockFlickrParser.photos))
                }
            }
        }
    }
}
