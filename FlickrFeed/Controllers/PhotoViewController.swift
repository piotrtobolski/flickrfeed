//
//  PhotoViewController.swift
//  FlickrFeed
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import UIKit
import AlamofireImage

class PhotoViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!

    var flickrPhoto: FlickrPhoto!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = flickrPhoto.title
        imageView.af_setImage(withURL: flickrPhoto.URL)
    }
}
