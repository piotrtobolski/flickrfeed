//
//  PhotoListViewModel.swift
//  FlickFeed
//
//  Created by Piotr Tobolski on 02.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Foundation

protocol PhotoListViewModelDelegate: class {
    func viewModelDidChangeStateTo(_ viewState: PhotoListViewModel.ViewState)
}

class PhotoListViewModel {
    enum ViewState {
        case `init`, loading, empty, ready
    }

    private(set) var photos = [FlickrPhoto]()
    private var viewState = ViewState.init {
        didSet {
            self.delegate?.viewModelDidChangeStateTo(viewState)
        }
    }

    weak var delegate: PhotoListViewModelDelegate? {
        didSet {
            self.delegate?.viewModelDidChangeStateTo(viewState)
        }
    }

    private var flickrFeedDownloader: FlickrFeedDownloader

    init(flickrFeedDownloader: FlickrFeedDownloader = FlickrFeedDownloader()) {
        self.flickrFeedDownloader = flickrFeedDownloader
    }

    var numberOfItems: Int {
        return photos.count
    }

    func load() {
        viewState = .loading

        DispatchQueue.global(qos: .userInitiated).async {
            self.photos = self.flickrFeedDownloader.downloadFeed()
            self.viewState = self.photos.isEmpty ? .empty : .ready
        }
    }
}
