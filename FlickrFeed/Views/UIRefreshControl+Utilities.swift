//
//  UIRefreshControl+Utilities.swift
//  FlickFeed
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import UIKit

extension UIRefreshControl {
    func makeSureIsRefreshing() {
        if !isRefreshing {
            beginRefreshing()
        }
    }

    func makeSureIsNotRefreshing() {
        if isRefreshing {
            endRefreshing()
        }
    }
}
