//
//  PhotoListCollectionViewCell.swift
//  FlickFeed
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import UIKit
import AlamofireImage
import FontAwesome_swift

class PhotoListCollectionViewCell: UICollectionViewCell {
    private static var placeholderImage = UIImage.fontAwesomeIcon(name: .image, textColor: .lightGray, size: CGSize(width: 100, height: 100))
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var placeholderImageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var authorLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        placeholderImageView.image = PhotoListCollectionViewCell.placeholderImage
    }

    override func prepareForReuse() {
        placeholderImageView.alpha = 1
        imageView.image = nil
        titleLabel.text = nil
        authorLabel.text = nil
    }

    func configure(with flickrPhoto: FlickrPhoto) {
        titleLabel.text = flickrPhoto.title
        authorLabel.text = flickrPhoto.author

        let imageTransition = UIImageView.ImageTransition.custom(duration: 0.5, animationOptions: .transitionCrossDissolve, animations: { imageView, image in
            imageView.image = image
            self.placeholderImageView.alpha = 0
        }, completion: nil)
        self.imageView.af_setImage(withURL: flickrPhoto.URL, imageTransition: imageTransition) { _ in
            self.placeholderImageView.alpha = 0
        }
    }
}
