//
//  FilckrPhoto.swift
//  FlickrFeed
//
//  Created by Piotr Tobolski on 02.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Foundation

struct FlickrPhoto: Equatable {
    let URL: URL

    let title: String?
    let author: String?
    let aspectRatio: Float?
}
