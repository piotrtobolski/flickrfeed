//
//  FlickrFeedDownloader.swift
//  FlickFeed
//
//  Created by Piotr Tobolski on 03.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Foundation
import FeedKit

private let feedURL = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne")!

protocol FeedParserProtocol {
    func parse() -> Result
}

extension FeedParser: FeedParserProtocol {}

class FlickrFeedDownloader {
    private let feedParserCreateBlock: () -> FeedParserProtocol
    private let flickrParser: FlickrParser

    init(feedParserCreateBlock: @escaping () -> FeedParserProtocol = { FeedParser(URL: feedURL)! }, flickrParser: FlickrParser = FlickrParser()) {
        self.feedParserCreateBlock = feedParserCreateBlock
        self.flickrParser = flickrParser
    }

    func downloadFeed() -> [FlickrPhoto] {
        let feedParser = feedParserCreateBlock()
        let result = feedParser.parse()
        guard case .atom(let feed) = result else {
            return [];
        }

        self.flickrParser.feed = feed
        let photos = self.flickrParser.getPhotos()
        return photos
    }
}
