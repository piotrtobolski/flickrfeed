//
//  NSString+Utilities.swift
//  FlickFeed
//
//  Created by Piotr Tobolski on 02.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Foundation

extension NSString {
    func getFloat(in range: NSRange) -> Float? {
        let substring = self.substring(with: range)
        return Float(substring)
    }
}
