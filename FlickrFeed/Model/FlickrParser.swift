//
//  FlickrParser.swift
//  FlickFeed
//
//  Created by Piotr Tobolski on 02.02.2018.
//  Copyright © 2018 Piotr Tobolski. All rights reserved.
//

import Foundation
import FeedKit

class FlickrParser {
    var feed: AtomFeed?

    init() {}

    func getPhotos() -> [FlickrPhoto] {
        guard let entries = feed?.entries else {
            return []
        }

        return entries.compactMap(parse(atomFeedEntry:))
    }

    private func parse(atomFeedEntry entry: AtomFeedEntry) -> FlickrPhoto? {
        guard let URL = entry.links?.compactMap(parse(atomFeedEntryLink:)).first else {
            return nil
        }

        let title = entry.title
        let author = entry.authors?.first?.name

        var aspectRatio: Float? = nil
        if let content = entry.content?.value {
            aspectRatio = parse(feedContent: content as NSString)
        }

        let photo = FlickrPhoto(URL: URL, title: title, author: author, aspectRatio: aspectRatio)

        return photo
    }

    private func parse(atomFeedEntryLink link: AtomFeedEntryLink) -> URL? {
        if let attributes = link.attributes,
            let URLString = attributes.href,
            attributes.rel == "enclosure" {

            return URL(string: URLString)
        }

        return nil
    }

    private func parse(feedContent: NSString) -> Float? {
        let reg = try! NSRegularExpression(pattern: "width=\"(\\d+)\" height=\"(\\d+)\"", options: [])

        let matches = reg.matches(in: feedContent as String, options: [], range: NSRange(location: 0, length: feedContent.length))

        guard
            matches.count == 1,
            let firstMatch = matches.first,
            firstMatch.numberOfRanges == 3,
            let width = feedContent.getFloat(in: firstMatch.range(at: 1)),
            let height = feedContent.getFloat(in: firstMatch.range(at: 2)),
            width > 0,
            height > 0
            else { return nil }

        return width / height
    }
}
